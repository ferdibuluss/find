﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Find.Models
{
    public class User
    {
        private int user_id;
        private String user_name;
        private String user_email;
        private int user_password;

        public User()
        {
        }

        public User(int user_id, string user_name, string user_email, int user_password)
        {
            this.User_name = user_name;
            this.User_email = user_email;
            this.User_password = user_password;
        }

        public string User_name { get => user_name; set => user_name = value; }
        public string User_email { get => user_email; set => user_email = value; }
        public int User_password { get => user_password; set => user_password = value; }
    }
}