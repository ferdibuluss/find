﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Find.Models
{
    public class Customer
    {
        private int customer_id;
        private String customer_name;
        private String customer_surname;
        private List<String> customer_interest;

        public Customer(int customer_id, string customer_name, string customer_surname, List<string> customer_interest)
        {
            this.Customer_name = customer_name;
            this.Customer_surname = customer_surname;
            this.Customer_interest = customer_interest;
        }

        public string Customer_name { get => customer_name; set => customer_name = value; }
        public string Customer_surname { get => customer_surname; set => customer_surname = value; }
        public List<string> Customer_interest { get => customer_interest; set => customer_interest = value; }
    }
}